package com.example.x

import androidx.room.Dao
import com.example.myapplication.entity.TestEntity

@Dao
interface TestEntityDao : IDao<TestEntity> {
}