package com.example.x

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.myapplication.entity.TestEntity

@Database(
    entities = [TestEntity::class],
    version = 7
)
abstract class XDataBase : RoomDatabase() {

    abstract fun getTestEntityDao(): TestEntityDao

}