package com.example.myapplication.repository

import com.example.myapplication.AppService
import com.example.myapplication.RequestEvent
import com.example.myapplication.datasource.TestEntityDataSource
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onCompletion
import kotlinx.coroutines.flow.onStart

class MainActivityRepository(
    private val appService: AppService,
    private val testEntityDataSource: TestEntityDataSource
) {
    fun request() = flow<RequestEvent<String>> {
//        emit(appService.requestData().apply {
//            testEntityDataSource.insert()
//        })
//        ==
//        val a = appService.requestData()
//        testEntityDataSource.insert()
        emit(RequestEvent.SuccessRequestEvent(data = "result"))
    }.onStart {
        emit(RequestEvent.StartRequestEvent())
    }.catch {
        emit(RequestEvent.FailRequestEvent(NullPointerException()))
    }.onCompletion {

    }
}