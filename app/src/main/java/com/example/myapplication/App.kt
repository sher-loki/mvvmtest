package com.example.myapplication

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin


class App : Application() {

    override fun onCreate() {
        super.onCreate()
        _app = this
        startKoin {
            this.androidContext(this@App)
            loadKoinModules(
                mutableListOf(
                    appViewModelModule, appServiceModule, appDataSourceModule,
                    appRepositoryModule, appExtraModule
                )
            )
        }
    }


    companion object {
        private var _app: App? = null
        val app: App
            get() = _app ?: throw RuntimeException("Have you invoke xxx")
    }

}