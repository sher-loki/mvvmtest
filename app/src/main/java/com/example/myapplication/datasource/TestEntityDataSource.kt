package com.example.myapplication.datasource

import com.example.myapplication.entity.TestEntity
import com.example.x.XDataBase

class TestEntityDataSource(
    private val xDataBase: XDataBase
) {

    suspend fun insert() {
        xDataBase.getTestEntityDao().insert(TestEntity(100, 100))
    }

}