package com.example.myapplication

import androidx.room.Room
import com.example.myapplication.datasource.TestEntityDataSource
import com.example.myapplication.repository.MainActivityRepository
import com.example.myapplication.viewmodel.MainActivityViewModel
import com.example.x.XDataBase
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val appViewModelModule = module {
    viewModel {
        MainActivityViewModel(get())
    }
}


val appServiceModule = module {
    single { get<ApiManager>().createService<AppService>("https://www.baidu.com/") }
}

val appDataSourceModule = module {
    single {
        TestEntityDataSource(get())
    }
}

val appRepositoryModule = module {
    single {
        MainActivityRepository(get(),get())
    }
}

val appExtraModule = module {
    single {
        ApiManager()
    }
    single {
        Room.databaseBuilder(get(), XDataBase::class.java, "xxxxxxH").build()
    }
}

