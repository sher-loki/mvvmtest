package com.example.myapplication.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.example.myapplication.repository.MainActivityRepository
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.launch

class MainActivityViewModel(
    private val mainActivityRepository: MainActivityRepository,
) : ViewModel() {


//    val liveData = MutableLiveData<Any>()

    fun request() {
//  mainActivityRepository.request()
//        livedata.
//        viewModelScope.launch {
//            mainActivityRepository.request().collect { value -> liveData.postValue(value) }
//        }
    }


    fun request2()  = mainActivityRepository.request().asLiveData()
}