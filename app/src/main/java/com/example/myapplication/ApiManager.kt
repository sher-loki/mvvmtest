package com.example.myapplication

import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit

class ApiManager internal constructor() {
    private val DEFAULT_TIMEOUT = 15_000L


    private val client: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .cache(Cache(File(app.cacheDir, "cache"), 1024 * 1024 * 100))
//            .cookieJar(cookieJar)
            .readTimeout(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
            .writeTimeout(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
            .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS)
            .apply {
//                val sslParams = getSslSocketFactory()
//                val sslParams = getSslSocketFactory(safeTrustManager)
//                val sslParams = getSslSocketFactory(app.assets.open("srca.cer"))
//                val sslParams = getSslSocketFactory(
//                    app.assets.open("xxx.bks"),
//                    "123456",
//                    app.assets.open("yyy.cer")
//                )
//                val sslParams = getSslSocketFactory(
//                    app.assets.open("xxx.bks"),
//                    "123456",
//                    safeTrustManager
//                )
//                sslParams.let {
//                    it.sSLSocketFactory?.let { factory ->
//                        it.trustManager?.let { manager ->
//                            sslSocketFactory(factory, manager)
//                        }
//                    }
//                }
//                hostnameVerifier(unSafeHostnameVerifier)
            }

//            .addNetworkInterceptor(ResponseProgressInterceptor().apply {
//                addUrl(ApiConstant.URL_UPLOAD_HEADER) { progress, isFinish ->
//                    "$progress  $isFinish".loge()
//                }
//            })

            .build()
    }

    fun <T> createService(clazz: Class<T>, baseUrl: String) =
        Retrofit
            .Builder()
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
            .create(clazz)

    inline fun < reified T> createService(baseUrl: String = "") =
        createService(T::class.java, baseUrl)

}