package com.example.myapplication.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "room_user_gift_table")
data class TestEntity(
    @PrimaryKey @ColumnInfo(name = "uin") var uin: Long = 0,
    @ColumnInfo(name = "receiveCount") var receiveCount: Int = 0,
)