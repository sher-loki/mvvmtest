package com.example.myapplication

import retrofit2.http.GET
import retrofit2.http.Url

interface AppService {
// https://www.wanandroid.com/article/list/0/json

    @GET
    suspend fun requestData(@Url url: String = "https://www.wanandroid.com/article/list/0/json"): Any

}