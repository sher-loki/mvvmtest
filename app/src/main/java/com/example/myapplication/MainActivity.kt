package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.myapplication.viewmodel.MainActivityViewModel
import org.koin.androidx.viewmodel.ext.android.activityViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


class AFragment : Fragment() {
    //  同一个
    private val viewModel by activityViewModel<MainActivityViewModel>()

    //   非同一个
//    private val viewModel by viewModel<MainActivityViewModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        viewModel.liveData.observe(viewLifecycleOwner){
//
//        }
    }
}

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainActivityViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.app_activity_main)

//        viewModel.liveData.observe(this){
//            Log.e("xxxxxxH","result -> $it")
//        }

        //onpause onresume
        viewModel.request2().observe(this) {
            when(it){
                is RequestEvent.StartRequestEvent->{
                    Log.e("xxxxxxH", "result -> start")

                }
                is RequestEvent.SuccessRequestEvent->{
                    Log.e("xxxxxxH", "result -> result")

                }
                is RequestEvent.FailRequestEvent->{
                    Log.e("xxxxxxH", "result -> fail")
                }
            }
        }

    }

    override fun onResume() {
        super.onResume()
//        viewModel.request()
    }

}